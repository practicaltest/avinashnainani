﻿using ImportExcelFileToDatabase.Models;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImportExcelFileToDatabase.Controllers
{
    public class UserOrderController : Controller
    {
        private ImporoItEntities db = new ImporoItEntities();
        //
        // GET: /UserOrder/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserOrder(HttpPostedFileBase FileUpload)
        {

            List<string> data = new List<string>();

            if (FileUpload != null)
            {
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {


                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Doc/");
                    FileUpload.SaveAs(targetpath + filename);
                    string pathToExcelFile = targetpath + filename;
                    var connectionString = "";

                    connectionString = ConfigurationManager.ConnectionStrings["ExcelConString"].ConnectionString;

                    var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                    var ds = new DataSet();

                    adapter.Fill(ds, "ExcelTable");

                    DataTable dtable = ds.Tables["ExcelTable"];

                    string sheetName = "Sheet1";

                    var excelFile = new ExcelQueryFactory(pathToExcelFile);
                    var sheetData = from a in excelFile.Worksheet<Models.UserOrder>(sheetName) select a;

                    foreach (var item in sheetData)
                    {
                        try
                        {
                            UserOrderData model = new UserOrderData();
                            model.OrderId = item.orderId;
                            model.paymenttype = item.paymenttype;
                            model.pintTypeId = item.pinTypeId;
                            model.price = item.price;
                            model.quanity = item.quanity;
                            model.productname = item.productname;
                            model.orderdate = item.orderdate;
                            model.customerName = item.customerName;
                            model.fullAddress = item.fulladdress;

                            db.UserOrderDatas.Add(model);

                            db.SaveChanges();

                        }
                        catch (DbEntityValidationException ex)
                        {
                            foreach (var entityValidationErrors in ex.EntityValidationErrors)
                            {

                                foreach (var validationError in entityValidationErrors.ValidationErrors)
                                {

                                    Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);

                                }

                            }
                        }
                    }
                }
            }
            return RedirectToAction("Index");
        }


    }
}


