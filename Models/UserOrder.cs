﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImportExcelFileToDatabase.Models
{
    public class UserOrder
    {
        public long orderId { get; set; }
        public int pinTypeId { get; set; }
        public int paymenttype { get; set; }
        public string customerName { get; set; }
        public string fulladdress { get; set; }
        public DateTime orderdate { get; set; }
        public decimal price { get; set; }
        public int quanity { get; set; }
        public string productname { get; set; }
    }
}